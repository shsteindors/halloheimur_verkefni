# Velkomin(n)! 
# Þessi Python forritsskrá er hugsuð sem verkefni fyrir byrjendur
# Hafðu engar áhyggjur, þetta er miklu skemmtilegra en flest verkefni ;)
# Mundu líka að prófa allt og að breyta hverju sem þér dettur í hug! 
# Einhver meistari sagði: 
# Ég heyri -> ég gleymi | Ég sé -> ég man | ég geri -> ég skil!

# Höfundur: Sverrir H. Steindórsson
# Byggt á gögnum frá Ben Nuttall
# https://github.com/bennuttall/python-intro
# Leyfi: Creative Commons 4.0 -> CC-BY-SA

# Línurnar sem byrja á kassa-/myllumerki (#) kallast athugasemdir (e. comments)
# Þær eru bara fyrir þig til að lesa og Python les þær ekki

# Þegar þú sérð |ÁFRAM!|, vistaðu og keyrðu skránna til að sjá úttakið
# Þegar þú sérð línu sem hefst á # fylgdu þá leiðbeiningunum
# Sumar línur hér að neðan eru python-kóði með # fyrir framan
# Þetta þýðir að þær eru hundsaðar sem athugasemdir
# Fjarlægðu # til að Python lesi línuna og keyri hana - auða bilið líka!
# Gerðu eina æfingu í einu, vistaðu og keyrðu síðan forritið í hvert skipti...

# 1. Þetta er print() fallið (e. function)

print("Halló heimur!")

# ÁFRAM!

# 2. Þetta er breyta (e. variable):

skilabod = "2. stig"

# Bættu við línu hér að neðan til að prenta þessa breytu

# ÁFRAM!

# 3. Breytan hér að ofan kallast strengur (e. string)
# Þú ræður hvort þú notar einfaldar eða tvöfaldar gæsalappir (mundu að loka þeim!)
# Þú getur spurt Python af hvaða tagi (e. type) einhver ákveðin breyta er...
# Prófaðu að þurrka út # hér að neðan

# print(type(skilabod))

# ÁFRAM!

# 4. Önnur tegund breyta eru heiltölur (engar kommur!)
a = 123
b = 654
c = a + b

# Prófaðu að prenta gildi breytunnar |c| til að sjá svarið

# ÁFRAM!

# 5. Þú getur notað aðra virkja eins og frádrátt (-) og margföldun (*)
# Prófaðu að skipta orðunum hér að neðan út fyrir samsvarandi virkja

# a sinnum b
# b mínus a
# 12 sinnum 4
# 103 plús 999

# ÁFRAM!

# 6. Breyta heldur gildi sínu þar til þú breytir því

a = 100
# print(a)  # hugsaðu... - ætti þetta að vera 123 eða 100?

c = 50
# print(c)  # hugsaðu... - er það 50 eða 777?

d = 10 + a - c
# print(d)  # hugsaðu... - hvað verður þetta núna?

# ÁFRAM!

# 7. Þú getur einnig notað |+| til að leggja saman tvo strengi

heilsa = 'Halló '
nafn = ''  # settu nafnið þitt í strenginn

skilabod = heilsa + nafn
# print(skilabod)

# ÁFRAM!

# 8. Ef þú prófar að leggja saman tölu og streng, færðu villuboð:

# aldur =  # settu þinn aldur sem gildi hérna (með tölustöfum)

# print(nafn + ' er ' + aldur + ' ára')

# ÁFRAM!

# Sérðu villuna? Við megum ekki blanda gagnatögum svona saman.
# En sérðu hvernig villuboðin benda þér á línuna sem inniheldur villu?
# Ekki örvænta þó þú fáir villuboð, allir fá þau við og við...
# Nú skaltu setja # fyrir framan línuna svo þú fáir ekki villuboð

# 9. Svona getum við breytt tölustöfum í strengi:

# print(nafn + ' er ' + str(aldur) + ' ára')

# ÁFRAM!

# Engin villuboð í þetta skiptið? Vonandi...

# Við gætum auðvitað líka slegið breytuna |aldur| inn sem streng:

# aldur =  # Sláðu inn aldur þinn, sem streng (vísbending: gæsalappir...)

# print(nafn + ' er ' + aldur + ' ára')

# ÁFRAM!

# Engin villuboð..?

# 10. Önnur tegund breyta eru tvíundabreytur (e. boolean)
# Tvíundabreyta getur einungis haft tvö gildi, annaðhvort True (ísl. Satt) eða False (ísl. Ósatt)

python_er_hressandi = True
python_er_of_erfitt = False

# Við getum líka borið saman tvær breytur með því að nota tvöfalt jafnaðarmerki (==)
# Eitt jafnaðarmerki (=) er fyrir gildisveitingu en tvö (==) fyrir samanburð (muna!)

aldur_siggu = 14
# aldur_minn =  # settu þinn aldur þarna inn

# print(aldur_minn == aldur_siggu)  # þessi lína skilar annað hvort True eða False

# ÁFRAM!

# 11. Við getum líka notað 'minna en' og 'stærra en' - það eru táknin < og >

# sigga_er_eldri = aldur_siggu > aldur_minn

# print(sigga_er_eldri)  # hvort helduru að þú fáir True eða False?

# ÁFRAM!

# 12. Við getum sett skilyrði með því að nota "ef-setningu" (e. if statement)
# Það er dálítið eins og að spyrja spurninga áður en við gerum eitthvað

peningar = 50000
simi_kostar = 24000
spjaldtolva_kostar = 20000

kostar_alls = simi_kostar + spjaldtolva_kostar
getur_keypt_allt = peningar > kostar_alls

if getur_keypt_allt:
    skilabod = "Þú átt fyrir þessu!"
else:
    skilabod = "Þú átt ekki fyrir öllu :("

# print(skilabod)  # hvað reiknar þú með að sjá?

# ÁFRAM!

# Breyttu nú verðinu á spjaldtölvunni |spjaldtölva_kostar| í 26000 og prófaðu aftur
# Hvaða skilaboð ættu núna að birtast?

# ÁFRAM!

# Er þetta rétt? Þú gætir þurft að breyta samanburðarvirkjanum í >=
# Þetta þýðir 'meira-en EÐA jafnt-og'

# ÁFRAM!

# 13. Það er til tegund breytu sem getur geymt mörg atriði...
# Þannig breyta kallast listi (e. list)

litir = ['Gulur', 'Rauður', 'Grænn', 'Blár', 'Svartur', 'Hvítur', 'Fjólublár']

# Við getum síðan kannað hvort ákveðinn litur sé í listanum

# print('Bleikur' in litir)  # Prentar annað hvort True eða False

# ÁFRAM!

# Við getum síðan bætt við listann með fallinu append()

litir.append('Brúnn')
litir.append('Bleikur')

# print('Bleikur' in litir)  # Ættum við að sjá breytingu núna?

# ÁFRAM!

# Við getum líka bætt heilum lista við annan lista með fallinu extend()

fleiri_litir = ['Grár', 'Appelsínugulur', 'Glær?']

litir.extend(fleiri_litir)

# Prófaðu núna að prenta listann til að sjá hvað er í honum...

# ÁFRAM!

# 14. Við getum einnig lagt saman tvo lista með + og búið þannig til nýjan:

megin_litir = ['Rauður', 'Grænn', 'Blár']
auka_litir = ['Gulur', 'Fjólublár', 'Túrkis']

lita_pakki = megin_litir + auka_litir

# Prófaðu að prenta listann lita_pakki

# 15. Við getum athugað hversu mörg atriði eru í listanum með fallinu len()
# Prófaðu það hér fyrir neðan...

# Hve margir litir voru í listanum lita_pakki?

# ÁFRAM!

allir_litir = litir + lita_pakki

# Hvað eru margir litir í allir_litir?
# Prófaðu það hérna og reyndu að keyra forritið í huganum fyrst...

# ÁFRAM!

# Fékkstu gildið sem þú áttir von á? Hvers vegna? / Hvers vegna ekki?

# 16. Skoðum aðeins fleira sem við getum gert við lista:

slettar_tolur = [2, 4, 6, 8, 10, 12]
deilir_3 = [3, 6, 9, 12]

tolur = slettar_tolur + deilir_3
# fallið len() skilar fjölda atriða:
# print(tolur, len(tolur))

# Við getum fjarlægt auka eintök af atriðum með set() fallinu...
# Síðan setjum nýju útgáfuna í nýja breytu:
tolur_set = set(tolur)
# print(tolur_set, len(tolur_set))
# Sérðu muninn?

# ÁFRAM!

# Prófum þetta með lita-listann okkar...
# litir_set = set(allir_litir)
# Hversu margir litir ætli verði í þessari breytu?
# Helduru að þeir verði jafn margir? Eða ekki? 
# Veltu þessu fyrir þér áður en þú prófar að prenta... (prófaðu líka len() )

# 17. Við getum notað lykkju til að sjá öll atriði innan lista

minn_bekkur = ['Sigga', 'Benni', 'Svenni', 'Gunna', 'Lilja', 'Gummi', 'Anna', 'Dabbi']

# Hér að neðan er marg-línu athugasemd (e. multi-line comment)
# Fjarlægðu gæsalappirnar (''') bæði fyrir og eftir kóðabálkinn
# Þá les Python for-lykkjuna (e. for-loop) - Þú gætir líka sett # fyrir framan ;)

'''
for nemandi in minn_bekkur:
    print(nemandi)
'''

# Prófaðu að bæta öllum félögum þínum á listann

# Rifjaðu upp muninn á append() og extend()... þú getur notað hvort tveggja!

# Næst ætlum við að prenta sætisnúmer allra atriða og notum til þess fallið index()

'''
for nemandi in minn_bekkur:
    print(minn_bekkur.index(nemandi))
'''
# Taktu eftir að Python byrjar á núll en ekki einum!
# Ef við viljum byrja á einum, þá þurfum við að bæta einum við...

'''
for nemandi in minn_bekkur:
	print(minn_bekkur.index(nemandi)+1)
'''

# Skrifum nú lykkju sem prentar tölustaf (byrjað á 1) á undan hverju nafni
# Nú gætum við t.d. sameinað það sem við höfum lært...

'''
for nemandi in minn_bekkur:
	print(minn_bekkur.index(nemandi)+1)
	print(nemandi)
'''
# Þetta er rétt en þetta er samt ekki nógu flott!
# Stundum þurfum við að huga að því hvernig upplýsingar birtast...
# Reynum þetta einu sinni enn.....

'''
for nemandi in minn_bekkur:
	print(minn_bekkur.index(nemandi)+1, end="")
	print(" " + nemandi)
'''
# Taktu eftir |end=""| í annari línu, merkir að við viljum ekki nýja línu eftir númerið
# Við bættum síðan |" "| í síðustu línuna til að fá bil á milli tölunnar og nafnsins

# ÁFRAM

# 18. Hvað er hægt að gera við strengi?

fullt_nafn = 'Sverrir Hrafn Steindórsson'

fyrsti_stafur = fullt_nafn[0]
sidasti_stafur = fullt_nafn[19]
fyrstu_3 = fullt_nafn[:3]  # [0:3 virkar líka]
sidustu_3 = fullt_nafn[-3:]  # [17:] og [17:20] virka líka
millinafn = fullt_nafn[8:14]

# Prófaðu að prenta þessar breytur og prófaðu líka að búa til orð úr einstaka stöfum!

# 19. Við getum líka skipt strengjum eftir ákveðu tákni með split() fallinu...

ein_setning = "Halló, ég heiti Sverrir"
partar = ein_setning.split(',')

# print(partar)
# print(type(partar))  # Af hvaða tagi er þessi breyta? Hvað getum við gert við hana?

# ÁFRAM!

long_setning = "Þetta er mjög mjög mjög mjög mjög löng setning"

# Prófaðu núna að skipta löngu setningunni og prentaðu síðan orðafjöldann

# ÁFRAM! (Það eru vísbendingar hér að neðan ef þú festist)

# Vísbending 1: Hvaða "tákn" (eða ekki-tákn) notum við til að skipta orðunum?
# Vísbending 2: Af hvaða tagi er skipta breytan?
# Vísbending 3: Hvað getum við gert til að telja? (Hugsaðu til baka...)

# 20. Við getum safnað gögnum saman í svokallaða "línu" (e. tuple)
# Þær geta geymt fleiri en eitt gildi
# Mundu samt að það er ekki hægt að breyta línum eftir að þær eru búnar til!

persona = ('Nonni', 26)

# print(persona[0] + ' er ' + str(persona[1]) + ' ára að aldri')

# ÁFRAM!

# (nafn, aldur)
nemendur = [
    ('Danni', 12),
    ('Sunna', 13),
    ('Simmi', 12),
    ('Kata', 11),
    ('Albert', 10)
]

# Nú skalt þú skrifa lykkju sem prentar nafn og aldur allra nemendanna

# ÁFRAM!

# 21. Línur (e. tuples) geta verið eins langar og okkur við viljum

# Prófaðu að búa til lista yfir nemendur sem inniheldur t.d. nafn, aldur, áhugamál, námsgrein...

# Skrifaðu síðan lykkju sem prentar öll gildin...

# Veldu síðan tölu í aldurs-dálknum...
# Skrifaðu lykkju sem prentar BARA þá sem eru eldri (en talan sem þú valdir)

# ÁFRAM!

# 22. Önnur gagnleg gagnaskipan (e. data structure) er orðasafn (e. dictionary)

# Orðasöfn geyma pöruð lykilgögn eins og t.d. símaskrár sem geyma nöfn og símanúmer

simaskra = {
    'Adda': '999 1337',
    'Binni': '314 1592',
    'Gugga': '141 4213',
    'Neyðarsími': '112'
}

# Við getum nálgast atriði úr [orða]safninu með því fletta þeim upp með lyklinum:

# print(simaskra['Gugga'])

# Við getum athugað hvort að ákveðinn lykill eða gildi sé til staðar í safninu:

# print('Bin Laden' in simaskra)  # [False]
# print('Binni' in simaskra)  # [True]
# print('112' in simaskra)  # [False]
# print('112' in simaskra.values())  # [True]
# print(112 in simaskra.values())  # [False]

# Athugaðu að 112 var fært inn í orðasafnið sem strengur, ekki sem heiltala
# Þess vegna verður 112 False en '112' True...

# ÁFRAM!

# Hugsaðu... Hvað myndi gerast ef símanúmerin væru geymd sem heiltölur?

# Prófaðu að breyta símanúmerinu hennar Guggu í annað númer

# simaskra['Gugga'] = '545 8400'
# print(simaskra['Gugga'])

# ÁFRAM!

# Eyddu núna 'Bin Laden' úr orðasafninu

# print('Bin Laden' in simaskra)  # [True]
# del simaskra['Bin Laden']
# print('Bin Laden' in simaskra)  # [False]

# ÁFRAM!

# Við getum líka notað lykkju til að prenta innihald orðasafnsins:

'''
for nafn in simaskra:
    print(nafn, simaskra[nafn])
'''

# ÁFRAM!

# 23. Þá er komið að síðustu þrautinni í þessu skjali...
# Hver er summa allra talna frá 1 og upp í 1000? (1 og 1000 teljast með!)

# ÁFRAM!

# Vísbending: range(10) => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
# Vísbending: str(87) => '87'
# Vísbending: int('9') => 9
